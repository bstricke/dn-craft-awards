#################################################################################
#																			    #
#  Script Title: Windows Update	Automation									    #
#  File Name: wu.ps1					      	   							    #
#  Written By: Brian Stricker  7/29/2015								        #
#  Description: PowerShell script to install Windows Updates and reboot.        #
#  Requirements: Power Shell Module PSWindowsUpdate which is available from     #
#  				 Microsoft Technet.											    #
#				 PowerShell 2.0 or better                                       #
#  Peer Review done by: 									                    #
#																			    #
#################################################################################
#																				#
# Determine Powershell Version													#
#																				#
$version= $PSVersionTable.PSVersion.Major
if ($version -lt 2) {Write-Host "This script requires PowerShell 2.0 or greater"}
if ($version -lt 2) {exit} 
#																				#
#################################################################################
#																				#
# Checking to see if PSWindowsUpdate module is present  						#
#																				#
if (Get-Module -ListAvailable -Name PSWindowsUpdate) { 
	Write-Host "The module PSWindowsUpdate exits"
	}
	else { 
	Write-Host "The module PSWindowsUpdate does not exist"
	Write-Host "Attempting to import the module PSWindowsUpdate"
    New-Item -Path C:\windows\System32\WindowsPowershell\v1.0\Modules\PSWindowsUpdate\ -ItemType directory -Force
	Copy-Item \\awswsus001\C$\PSScripts\PSWindowsUpdate\  C:\Windows\System32\WindowsPowerShell\v1.0\Modules\ -Recurse -Force
	Import-Module PSWindowsUpdate
	}
#																				#
#################################################################################
#																				#
# Install windows Updates and reboot											#
#																				#
#                                       										#
Get-WUInstall -ServiceID 3da21691-e39d-4da6-8a4b-b43877bcb1b7 -AcceptALL -Autoreboot
Get-WUHistory | Out-File c:\WUHistory.txt
#																				#
#################################################################################
#																				#
exit
# End of Script																	#
#																				#
#################################################################################